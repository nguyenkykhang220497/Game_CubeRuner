﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    public GameObject obstacle;
    public float spawnTime;

    float m_spawnTime;
    int m_score;
    bool m_isGameOver;
    bool m_isGameStart;
    UIManager m_ui;

    // Start is called before the first frame update
    void Start()
    {
        m_ui = FindObjectOfType<UIManager>();
        m_ui.ShowGameStartPanel(true);
        m_ui.SetScoreText("Score : " + m_score);
    }

    // Update is called once per frame
    void Update()
    {
        if(m_isGameOver)
        {
            m_spawnTime = 0;
            return;
        }

        if(m_isGameStart)
        {
            m_spawnTime -= Time.deltaTime;

            if (m_spawnTime <= 0)
            {
                SpawnObstacle();
                if(m_score >= 50)
                {
                    m_spawnTime = spawnTime - 3.5f;
                }
                else if(m_score >= 30)
                {
                    m_spawnTime = spawnTime - 3;
                }
                else if(m_score >= 20)
                {
                    m_spawnTime = spawnTime - 2;
                }
                else if (m_score >= 10)
                {
                    m_spawnTime = spawnTime - 1;
                }else {

                    m_spawnTime = spawnTime;
                } 
            }
        }

    }

    public void OnStart()
    {
        m_spawnTime = 0;
        m_score = 0;
        m_isGameStart = true;
        m_ui.ShowGameStartPanel(false);
    }

    public void Replay()
    {
        //m_spawnTime = 0;
        //m_score = 0;
        //m_isGameOver = false;
        //m_ui.SetScoreText("Score : " + m_score);
        //m_ui.ShowGameOverPanel(false);
        SceneManager.LoadScene("GamePlay");
    }

    public void SpawnObstacle()
    {
        Vector2 newObstacle = new Vector2(11, Random.Range(-3f, -1.9f));

        if(obstacle)
        {
            Instantiate(obstacle, newObstacle, Quaternion.identity);
        }

    }

    public void IncrementScore()
    {
        if (m_isGameOver) return;
        m_score++;
        m_ui.SetScoreText("Score : " + m_score);
    }

    public void SetScore(int value)
    {
        m_score = value;
       
    }

    public int GetScore() => m_score;

    public void SetGameOver(bool state)
    {
        m_isGameOver = state;
        if (state)
        {
            m_ui.ShowGameOverPanel(true);
        }
    }

    public bool GetGameOver() => m_isGameOver;
}
