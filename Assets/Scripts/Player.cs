﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float jumForce;
    public AudioSource aus;
    public AudioClip jumAudio;
    public AudioClip loseAudio;

    Rigidbody2D m_rb;
    GameController m_gc;

    bool m_isGround;

    // Start is called before the first frame update
    void Start()
    {
        m_rb = GetComponent<Rigidbody2D>();
        m_gc = FindObjectOfType<GameController>();
    }

    // Update is called once per frame
    void Update()
    {
        bool isJumKeyPressed = Input.GetKeyDown(KeyCode.Space);

        if(isJumKeyPressed && m_isGround)
        {
            if (aus && jumAudio )
            {
                aus.PlayOneShot(jumAudio);
            }
            m_rb.AddForce(Vector2.up * jumForce);
            m_isGround = false;
        }
    }

    private void OnCollisionEnter2D(Collision2D col)
    {
        if(col.gameObject.CompareTag("Ground"))
        {
            m_isGround = true;
        }
    }

    private void OnTriggerEnter2D(Collider2D col)
    {
        if(col.CompareTag("Obstacle"))
        {
            if (aus && loseAudio && !m_gc.GetGameOver())
            {
                aus.PlayOneShot(loseAudio);
            }
            m_gc.SetGameOver(true);
        }
    }
}
