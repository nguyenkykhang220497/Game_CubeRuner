﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public Text scoreText;

    public GameObject gameOverPanel;

    public GameObject gameStartPanel;


    public void SetScoreText(string txt)
    {
        if(scoreText)
        {
            scoreText.text = txt;
        }
    }

    public void ShowGameOverPanel(bool state)
    {
        if(gameOverPanel)
        {
            gameOverPanel.SetActive(state);
        }
    }

    public void ShowGameStartPanel(bool state)
    {
        if (gameStartPanel)
        {
            gameStartPanel.SetActive(state);
        }
    }
}

